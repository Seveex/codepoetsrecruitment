import os
from app import app
from flask import render_template, request, redirect, url_for, session, Flask
from werkzeug.utils import secure_filename
import math
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg') # solves problems with matplotlib + flask
import seaborn as sns
from scipy.stats import chi2_contingency


SEPARATOR = '\t'
DIGITS = [1, 2, 3, 4, 5, 6, 7, 8, 9]
alpha = 0.05

UPLOAD_FOLDER = './uploads/'
RESULTS_FOLDER = './venv/app/static/results/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

app.secret_key = 'totally secret'


@app.route('/')
@app.route('/index', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        uploaded_file = request.files['file']
        if uploaded_file.filename != '':
            filename = secure_filename(uploaded_file.filename)
            column = int(request.form['column'])
            if request.form.get('if_labels') == 'no':
                if_labels = 0
            else:
                if_labels = 1
            path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            uploaded_file.save(path)
            benfords_count, benfords_percent, usefull_data_percent, usefull_data_count = read_file(path, column, if_labels)
            validation, p_value = validate_benfords_law(benfords_count, usefull_data_count)
            make_graph(benfords_count, benfords_percent, usefull_data_count, filename)
            #benfords_count = ' '.join([str(elem) for elem in benfords_count])
            path_to_graph = os.path.join('/static/results/', filename + '.png')
            session['benfords_results'] = [benfords_count, usefull_data_percent, path_to_graph, validation, p_value]
        #else:
            #session.clear()
        return redirect(url_for('index'))
    return render_template('index.html')
#

def read_file(path, column, if_labels):
    file = open(path, 'r')
    lines = file.readlines()
    benfords_count = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    usefull_data_count = 0
    # I assume we want to check Benfords law only for positive integers. Other types (fe. entered by mistake) are omitted.
    # For reasonably large dataset (and it is not reliable to check Benford's law for small datasets)
    # this difference in number of samples neglectable.
    # Additionally the procentage of valid samples is calculated (just for info, but can be used for validation).
    for x in lines[if_labels:]:
        if x.split(SEPARATOR)[column - 1].isnumeric():
            benfords_count[int(x.split(SEPARATOR)[column - 1][0]) - 1] += 1
            usefull_data_count +=1
    usefull_data_percent = usefull_data_count / (len(lines) - if_labels) * 100
    if usefull_data_count > 0:
        benfords_percent = [x / usefull_data_count for x in benfords_count]
    else:
        benfords_percent = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    file.close()
    return benfords_count, benfords_percent, usefull_data_percent, usefull_data_count


def benfords_law(d):
    return math.log(1 + 1/d, 10)


def make_graph(benfords_count, benfords_percent, usefull_data_count, filename):
    sns.set(style="white", rc={"lines.linewidth": 3})
    fig, ax1 = plt.subplots(figsize=(5,5))
    ax2 = ax1.twinx()

    # theoretical values when benfords law holds
    # explanation why x axis values are converted to string
    # https://stackoverflow.com/questions/64968987/seaborn-plotting-3-lines-in-1-plot-one-of-it-shifts-right
    sns.lineplot(x=[str(d) for d in DIGITS], y=[benfords_law(d) for d in DIGITS], color='r', marker='o', ax=ax1)
    # calculated values from the dataset
    sns.barplot(x=DIGITS, y=benfords_percent, color='#004488', ax=ax1)
    sns.barplot(x=DIGITS, y=benfords_count, color='#004488', alpha=0, ax=ax2)
    # values above the bars
    for p in ax2.patches:
        ax2.annotate("%g" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
                    ha='center', va='center', fontsize=8, color='white', xytext=(0, -20),
                    textcoords='offset points')
    #_ = ax2.set_ylim(0, 6500)  # To make space for the annotations - no need, moreover not a good idea

    ax1.set(xlabel='Leading digit', ylabel='Frequency [%]', title="Benford's Law Valadation for "+filename)
    ax2.set(ylabel='Number of appearances')

    #plt.show()
    plt.savefig(RESULTS_FOLDER + filename + '.png', bbox_inches='tight')
    plt.close()


def validate_benfords_law(benfords_count, usefull_data_count):
    if any(x == 0 for x in benfords_count):
        return False, None
    c, p, dof, expected = chi2_contingency([benfords_count, [int(benfords_law(d)*usefull_data_count) for d in DIGITS]])
    if p >= alpha:
        return True, p
    else:
        return False, p
